document.getElementById("findButton").onclick = function() {
    let anagrams = []
    for (let word of words) {
        const typedText = document.getElementById("input").value;
        function alphabetize(a) {
            return a.toLowerCase().split("").sort().join("").trim();
        }
        let orderedText = alphabetize(typedText)
        if(orderedText.length === word.length && orderedText === alphabetize(word)){
            anagrams.push(word)
        }
    }
    createNewDiv(anagrams.join(", "))
}
function createNewDiv(content){
    let newDiv= document.createElement('div');
    let textNode= document.createTextNode(content)
    newDiv.appendChild(textNode);
    let destination = document.getElementById('d1');
    destination.appendChild(newDiv);}
